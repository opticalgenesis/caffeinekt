package jfelt.lpb.caffeinekt.espressomachines

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.BeanRecyclerItemBinding
import jfelt.lpb.caffeinekt.databinding.FragmentEspressoMachinesMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class EspressoMachineFragmentMain : Fragment() {

    private var _binding: FragmentEspressoMachinesMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEspressoMachinesMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = EspressoMachineAdapter(requireContext())
        val llm = LinearLayoutManager(requireContext())
        val decor = DividerItemDecoration(requireContext(), llm.orientation)
        val rv = binding.fragmentEspressoMachineRv
        rv.adapter = adapter
        rv.layoutManager = llm
        rv.addItemDecoration(decor)

        val vm: EspressoMachineViewModel = ViewModelProvider(this)[EspressoMachineViewModel::class.java]
        vm.allMachines.observe(viewLifecycleOwner, Observer {
            adapter.setMachineList(it)
        })
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private inner class EspressoMachineAdapter internal constructor(val c: Context) : RecyclerView.Adapter<EspressoMachineAdapter.MachineHolder>() {
        var machines = emptyList<EspressoMachine>()

        private val dao = EspressoMachineDatabase.getInstance(c).espressoMachineDao()
        private val repo = EspressoMachineRepo(dao)

        private inner class MachineHolder(v: View) : RecyclerView.ViewHolder(v) {
            val layout: ConstraintLayout = v.findViewById(R.id.bean_item_layout)
            val manufacturer: TextView = v.findViewById(R.id.bean_item)
            val model: TextView = v.findViewById(R.id.bean_item_roaster)
        }

        override fun getItemCount() = machines.size

        override fun onBindViewHolder(holder: MachineHolder, position: Int) {
            holder.manufacturer.text = machines[position].manufacturer
            holder.model.text = machines[position].model

            holder.layout.setOnLongClickListener {
                val popup = PopupMenu(c, it)
                popup.menuInflater.inflate(R.menu.menu_item_popup, popup.menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.popup_delete -> {
                            MainScope().launch(Dispatchers.IO) {
                                repo.deleteEspressoMachine(machines[position])
                            }
                            true
                        } else -> false
                    }
                }
                popup.show()
                true
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            MachineHolder(BeanRecyclerItemBinding.inflate(LayoutInflater.from(c), parent, false).root)

        fun setMachineList(m: List<EspressoMachine>) {
            machines = m
            notifyDataSetChanged()
        }
    }
}
package jfelt.lpb.caffeinekt.espressomachines

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

@Entity(tableName = "espresso_machines")
data class EspressoMachine(@PrimaryKey(autoGenerate = true) var id: Int?, val manufacturer: String, val model: String) {
    override fun toString() = "$manufacturer $model"
}

class EspressoMachineConverter {
    @TypeConverter
    fun fromMachine(m: EspressoMachine): String {
        val t = object : TypeToken<EspressoMachine>(){}.type
        return Gson().toJson(m, t)
    }

    @TypeConverter
    fun toMachine(s: String): EspressoMachine {
        val t = object : TypeToken<EspressoMachine>(){}.type
        return Gson().fromJson(s, t)
    }
}

@Dao
interface EspressoMachineDao {
    @Query("SELECT * FROM espresso_machines")
    fun getAllMachines(): LiveData<List<EspressoMachine>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(m: EspressoMachine)

    @Delete
    suspend fun delete(m: EspressoMachine)
}

@Database(entities = [EspressoMachine::class], version = 1, exportSchema = false)
@TypeConverters(EspressoMachineConverter::class)
abstract class EspressoMachineDatabase : RoomDatabase() {
    abstract fun espressoMachineDao(): EspressoMachineDao

    companion object {
        @Volatile
        var INSTANCE: EspressoMachineDatabase? = null

        fun getInstance(c: Context): EspressoMachineDatabase {
            val tmp = INSTANCE
            if (tmp != null) return tmp

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    EspressoMachineDatabase::class.java,
                    "espresso_machine_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
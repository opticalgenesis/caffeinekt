package jfelt.lpb.caffeinekt.espressomachines

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import jfelt.lpb.caffeinekt.databinding.DialogNewGrinderBinding

class NewEspressoMachineDialog(private val li: OnEspressoMachineAddedListener) : DialogFragment() {

    var _binding: DialogNewGrinderBinding? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = DialogNewGrinderBinding.inflate(LayoutInflater.from(requireContext()))
        return MaterialAlertDialogBuilder(requireContext()).apply {
            setTitle("New Espresso Machine")
            setView(binding.root)
            setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            // TODO add watchers for verification
            setPositiveButton("Add") { dialog, _ ->
                val machine = EspressoMachine(null, binding.dialogGrinderManufacturer.editText?.text?.toString()!!,
                                                binding.dialogGrinderModel.editText?.text?.toString()!!)
                li.onMachineAdded(machine)
                dialog.dismiss()
            }
        }.create()
    }

    interface OnEspressoMachineAddedListener {
        fun onMachineAdded(m: EspressoMachine)
    }
}
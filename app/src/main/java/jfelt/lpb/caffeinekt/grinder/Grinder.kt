package jfelt.lpb.caffeinekt.grinder

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

@Entity(tableName = "grinders")
data class Grinder(@PrimaryKey(autoGenerate = true) var id: Int?, val manufacturerName: String, val modelName: String) {
    override fun toString() = "$manufacturerName $modelName"
}

class GrinderConverter {
    @TypeConverter
    fun fromGrinder(g: Grinder): String {
        val t = object : TypeToken<Grinder>(){}.type
        return Gson().toJson(g, t)
    }

    @TypeConverter
    fun fromJson(j: String): Grinder {
        val t = object : TypeToken<Grinder>(){}.type
        return Gson().fromJson(j, t)
    }
}

@Dao
interface GrinderDao {
    @Query("SELECT * FROM grinders")
    fun getGrinders(): LiveData<List<Grinder>>

    // TODO ???
    @Query("SELECT * FROM grinders WHERE :g")
    fun getGrinder(g: Grinder): LiveData<Grinder>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(g: Grinder)

    @Delete
    suspend fun delete(g: Grinder)
}

@Database(entities = [Grinder::class], version = 1, exportSchema = false)
@TypeConverters(GrinderConverter::class)
abstract class GrinderDatabase : RoomDatabase() {
    abstract fun grinderDao(): GrinderDao

    companion object {
        @Volatile
        var INSTANCE: GrinderDatabase? = null

        fun getInstance(c: Context): GrinderDatabase {
            val tmp = INSTANCE
            if (tmp != null) return tmp

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    GrinderDatabase::class.java,
                    "grinder_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

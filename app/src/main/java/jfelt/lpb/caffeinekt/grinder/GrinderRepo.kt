package jfelt.lpb.caffeinekt.grinder

import androidx.lifecycle.LiveData

class GrinderRepo(private val dao: GrinderDao) {
    val allGrinders: LiveData<List<Grinder>> = dao.getGrinders()

    suspend fun insertGrinder(g: Grinder) = dao.insert(g)

    suspend fun deleteGrinder(g: Grinder) = dao.delete(g)
}
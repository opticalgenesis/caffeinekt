package jfelt.lpb.caffeinekt

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import jfelt.lpb.caffeinekt.aeropress.AeropressBrewActivity
import jfelt.lpb.caffeinekt.aeropress.AeropressMainFragment
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanDatabase
import jfelt.lpb.caffeinekt.bean.BeanRepo
import jfelt.lpb.caffeinekt.bean.NewBeanDialog
import jfelt.lpb.caffeinekt.databinding.ActivityMainBinding
import jfelt.lpb.caffeinekt.databinding.DialogNewBeanBinding
import jfelt.lpb.caffeinekt.espresso.EspressoActivity
import jfelt.lpb.caffeinekt.grinder.*
import jfelt.lpb.caffeinekt.pourover.PouroverActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), View.OnClickListener, NewBeanDialog.OnBeanAddedListener, NewGrinderDialog.OnGrinderAddedListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var fabMain: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = binding.activityMainToolbar
        setSupportActionBar(toolbar)

        val drawer = binding.activityMainDrawer
        val navView = binding.activityMainNavView
        toolbar.setNavigationOnClickListener {
            if (drawer.isDrawerOpen(navView)) {
                drawer.closeDrawer(navView)
            } else {
                drawer.openDrawer(navView)
            }
        }

        AppBarConfiguration(findNavController(R.id.activity_main_fragment_container).graph, binding.activityMainDrawer)
        binding.activityMainNavView.setupWithNavController(findNavController(R.id.activity_main_fragment_container))

        fabMain = binding.activityMainFab
        fabMain.setOnClickListener(this)
        setupDials()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.activity_main_fab -> toggleExpandedFab()
            R.id.activity_main_new_grinder -> {
                val d = NewGrinderDialog(this)
                d.show(supportFragmentManager, d.tag)
                toggleExpandedFab()
            }
            R.id.activity_main_new_bean -> {
                val d = NewBeanDialog(this, null)
                d.show(supportFragmentManager, d.tag)
                toggleExpandedFab()
            }
            R.id.activity_main_new_aeropress_brew -> {
                startActivity(Intent(this, AeropressBrewActivity::class.java))
                toggleExpandedFab()
            }
            R.id.activity_main_new_filter_brew -> {
                startActivity(Intent(this, PouroverActivity::class.java))
                toggleExpandedFab()
            }
            R.id.activity_main_new_espresso_brew -> {
                startActivity(Intent(this, EspressoActivity::class.java))
                toggleExpandedFab()
            }
        }
    }

    override fun onBeanAdded(b: Bean) {
        Log.d("BEAN", "$b")
        val db = BeanDatabase.getDatabase(this)
        val dao = db.beanDao()
        val repo = BeanRepo(dao)
        MainScope().launch(Dispatchers.IO) { repo.addNewBean(b) }
    }

    override fun onGrinderAdded(g: Grinder) {
        val db = GrinderDatabase.getInstance(this)
        val dao = db.grinderDao()
        val repo = GrinderRepo(dao)
        MainScope().launch(Dispatchers.IO) { repo.insertGrinder(g) }
    }

    fun hideFab() { fabMain.visibility = View.INVISIBLE }

    private fun toggleExpandedFab() {
        // TODO disabled while testing BSD
        fabMain.isExpanded = !fabMain.isExpanded
    }

    private fun setupDials() {
        val dialArr = arrayOf(binding.activityMainNewGrinder, binding.activityMainNewBean, binding.activityMainNewEspressoBrew, binding.activityMainNewAeropressBrew, binding.activityMainNewFilterBrew)
        dialArr.all {
            it.setOnClickListener(this)
            true
        }
    }
}
package jfelt.lpb.caffeinekt.pourover

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanConverter

@Entity(tableName = "pourover_brews")
data class Pourover(@PrimaryKey(autoGenerate = true) val id: Int?, val bean: Bean, val brewer: String, var bloomTime: Int? = null,
                    val coffeeMass: Float, val waterMass: Float, val pours: List<Pour>)

@Dao
interface PouroverDao {
    @Query("SELECT * FROM pourover_brews")
    fun getAllPourovers(): LiveData<List<Pourover>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(p: Pourover)

    @Delete
    fun delete(p: Pourover)
}

@Database(entities = [Pourover::class], version = 1, exportSchema = false)
@TypeConverters(BeanConverter::class, PourConverter::class)
abstract class PouroverDatabase : RoomDatabase() {
    abstract fun pouroverDao(): PouroverDao

    companion object {
        @Volatile
        var INSTANCE: PouroverDatabase? = null

        fun getInstance(c: Context): PouroverDatabase {
            val tmp = INSTANCE
            if (tmp != null) return tmp

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    PouroverDatabase::class.java,
                    "pourover_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
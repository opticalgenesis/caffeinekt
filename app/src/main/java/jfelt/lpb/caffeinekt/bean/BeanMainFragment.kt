package jfelt.lpb.caffeinekt.bean

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.FragmentBeanMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class BeanMainFragment : Fragment() {

    private var _binding: FragmentBeanMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var beanVm: BeanViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBeanMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv: RecyclerView = binding.fragmentBeanMainRecycler
        rv.layoutManager = LinearLayoutManager(requireContext())
        val adapter = BeanAdapter(requireContext())
        rv.adapter = adapter

        val decor = DividerItemDecoration(requireContext(), (rv.layoutManager as LinearLayoutManager).orientation)
        rv.addItemDecoration(decor)

        beanVm = ViewModelProvider(this).get(BeanViewModel::class.java)
        beanVm.allBeans.observe(requireActivity(), Observer { w ->
            w?.let { adapter.setBeans(it) }
        })
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).supportActionBar?.title = "Beans"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    inner class BeanAdapter internal constructor(private val c: Context) : RecyclerView.Adapter<BeanAdapter.BeanHolder>() {

        private val inf = LayoutInflater.from(c)
        private var beans = emptyList<Bean>()

        inner class BeanHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val v: TextView = itemView.findViewById(R.id.bean_item)
            val deets: TextView = itemView.findViewById(R.id.bean_item_roaster)
            val parent: ConstraintLayout = itemView.findViewById(R.id.bean_item_layout)
        }

        override fun getItemCount() = beans.size


        // TODO -- STORE REFERENCE TO LAST DELETED ITEM AND ALLOW FOR UNDO VIA SNACKBAR
        override fun onBindViewHolder(holder: BeanHolder, position: Int) {
            holder.parent.setOnLongClickListener {
                val popup = PopupMenu(c, it)
                popup.menuInflater.inflate(R.menu.menu_item_popup, popup.menu)
                popup.setOnMenuItemClickListener {item ->
                    when (item.itemId) {
                        R.id.popup_delete -> {
                            val db = BeanDatabase.getDatabase(c)
                            val repo = BeanRepo(db.beanDao())
                            MainScope().launch(Dispatchers.IO) {repo.removeBean(beans[position])}
                            true
                        }
                        else -> false
                    }
                }
                popup.show()
                true
            }
            holder.v.text = beans[position].name
            holder.deets.text = "${beans[position].roaster} - ${beans[position].roastProfile}"
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            BeanHolder(inf.inflate(R.layout.bean_recycler_item, parent, false))

        internal fun setBeans(beans: List<Bean>) {
            this.beans = beans
            notifyDataSetChanged()
        }
    }
}
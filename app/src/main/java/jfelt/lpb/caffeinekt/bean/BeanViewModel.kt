package jfelt.lpb.caffeinekt.bean

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BeanViewModel(app: Application) : AndroidViewModel(app) {
    private val repo: BeanRepo

    val allBeans: LiveData<List<Bean>>

    init {
        val beanDao = BeanDatabase.getDatabase(app).beanDao()
        repo = BeanRepo(beanDao)
        allBeans = repo.liveBeans
    }

    fun addNewBean(b: Bean) = viewModelScope.launch(Dispatchers.IO) { repo.addNewBean(b) }
}
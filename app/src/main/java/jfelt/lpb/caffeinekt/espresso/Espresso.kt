package jfelt.lpb.caffeinekt.espresso

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import jfelt.lpb.caffeinekt.bean.Bean
import jfelt.lpb.caffeinekt.bean.BeanConverter
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachine
import jfelt.lpb.caffeinekt.espressomachines.EspressoMachineConverter
import jfelt.lpb.caffeinekt.grinder.Grinder
import jfelt.lpb.caffeinekt.grinder.GrinderConverter

@Entity(tableName = "espresso_brews")
data class Espresso(@PrimaryKey(autoGenerate = true) var id: Int?, val bean: Bean, val machine: EspressoMachine, val grinder: Grinder,
                    var preinfusionTime: Int? = 0, val dose: Float, val outputMass: Float, val targetTime: Int?, val rating: Int?, val notes: String?) {
    override fun toString(): String {
        // TODO
        return super.toString()
    }
}

class EspressoConverter {
    @TypeConverter
    fun fromEspressoBrew(e: Espresso): String {
        val t = object : TypeToken<Espresso>(){}.type
        return Gson().toJson(e, t)
    }

    @TypeConverter
    fun fromJson(j: String): Espresso {
        val t = object : TypeToken<Espresso>(){}.type
        return Gson().fromJson(j, t)
    }
}

@Dao
interface EspressoDao {
    @Query("SELECT * FROM espresso_brews")
    fun getAllEspressoBrews(): LiveData<List<Espresso>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(e: Espresso)

    @Delete
    fun delete(e: Espresso)
}

@Database(entities = [Espresso::class], version = 1, exportSchema = false)
@TypeConverters(GrinderConverter::class, BeanConverter::class, EspressoMachineConverter::class)
abstract class EspressoDatabase : RoomDatabase() {
    abstract fun espressoDao(): EspressoDao

    companion object {
        @Volatile
        var INSTANCE: EspressoDatabase? = null

        fun getInstance(c: Context): EspressoDatabase {
            val tmp = INSTANCE
            if (tmp != null) return tmp

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    c.applicationContext,
                    EspressoDatabase::class.java,
                    "espresso_brew_db"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

package jfelt.lpb.caffeinekt.aeropress

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AeropressViewModel(app: Application) : AndroidViewModel(app) {

    private val repo: AeropressRepo

    val allBrews: LiveData<List<Aeropress>>

    init {
        val dao = AeropressBrewDatabase.getInstance(app).aeropressDao()
        repo = AeropressRepo(dao)
        allBrews = repo.liveBrews
    }

    fun addNewBrew(a: Aeropress) = viewModelScope.launch(Dispatchers.IO) { repo.addNewBrew(a) }
}
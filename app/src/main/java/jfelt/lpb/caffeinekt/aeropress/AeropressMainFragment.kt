package jfelt.lpb.caffeinekt.aeropress

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import jfelt.lpb.caffeinekt.MainActivity
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.databinding.FragmentAeropressMainBinding

class AeropressMainFragment : Fragment() {

    private var _binding: FragmentAeropressMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var vm: AeropressViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAeropressMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val parent = requireActivity() as MainActivity
        parent.supportActionBar?.title = "Aeropress"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val a = AeropressBrewAdapter(requireContext())
        val llm = LinearLayoutManager(requireContext())
        val decor = DividerItemDecoration(requireContext(), llm.orientation)
        val rv = binding.fragmentAeropressPreviousBrews

        rv.layoutManager = llm
        rv.addItemDecoration(decor)
        rv.adapter = a

        vm = ViewModelProvider(this).get(AeropressViewModel::class.java)
        vm.allBrews.observe(requireActivity(), Observer {
            it?.let { a.setBrews(it) }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    inner class AeropressBrewAdapter internal constructor(private val c: Context)
        : RecyclerView.Adapter<AeropressBrewAdapter.BrewHolder>() {

        private val inf = LayoutInflater.from(c)
        private var brews = emptyList<Aeropress>()

        inner class BrewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            // TODO replace with dedicated brewholder
            val v: ConstraintLayout = itemView.findViewById(R.id.bean_item_layout)
            val name: TextView = itemView.findViewById(R.id.bean_item)
            val deets: TextView = itemView.findViewById(R.id.bean_item_roaster)
        }

        override fun getItemCount() = brews.size

        override fun onBindViewHolder(holder: BrewHolder, position: Int) {
            holder.name.text = brews[position].bean.name
            val isInverted = if (brews[position].bIsInverted) "Inverted" else "Upright"
            holder.deets.text = "$isInverted - ${brews[position].bloomTime + brews[position].steepTime}"
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            BrewHolder(inf.inflate(R.layout.bean_recycler_item, parent, false))

        internal fun setBrews(brews: List<Aeropress>) {
            this.brews = brews
            notifyDataSetChanged()
        }
    }
}
package jfelt.lpb.caffeinekt.aeropress

import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import jfelt.lpb.caffeinekt.R
import jfelt.lpb.caffeinekt.bean.*
import jfelt.lpb.caffeinekt.databinding.ActivityAeropressNewBrewBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AeropressBrewActivity : AppCompatActivity(), NewBeanDialog.OnBeanAddedListener {

    private lateinit var binding: ActivityAeropressNewBrewBinding

    private var currentBrewTime: Int = 0

    private lateinit var beanVm: BeanViewModel
    private lateinit var existingBeanCheckBox: CheckBox

    private lateinit var selectedBean: Bean
    private var bIsInverted: Boolean = true
    private var bloomTime: Int = 0
    private var steepTime: Int = 0
    private var coffeeWeight: Float = 0.0f
    private var waterWeight: Float = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAeropressNewBrewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.activityAeropressBrewToolbar)

        supportActionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            subtitle = "New Brew"
        }

        initBeansList()

        val addBeanBtn = binding.aeropressBrewAddNewBeanButton

        existingBeanCheckBox = binding.aeropressBrewNewBean

        existingBeanCheckBox.setOnCheckedChangeListener {_, isChecked ->
            if (isChecked) {
                binding.aeropressChooseBeanLayout.visibility = View.VISIBLE
                addBeanBtn.visibility = View.INVISIBLE
            } else {
                binding.aeropressChooseBeanLayout.visibility = View.GONE
                addBeanBtn.visibility = View.VISIBLE
            }
        }

        addBeanBtn.setOnClickListener {
            val bd = NewBeanDialog(this, null)
            bd.show(supportFragmentManager, bd.tag)
        }

        binding.aeropressBrewInverted.setOnCheckedChangeListener { _, isChecked -> bIsInverted = isChecked }

        binding.aeropressBrewContinue.setOnClickListener {
            if (validateRequiredInputs()) {
                saveBrew()
            } else {
                Snackbar.make(binding.aeropressBrewLayout, "Ensure all required fields are complete!", Snackbar.LENGTH_SHORT).show()
            }
        }

        // TODO add brew tracking
    }

    override fun onBeanAdded(b: Bean) {
        beanVm.addNewBean(b)
        existingBeanCheckBox.isChecked = true
    }

    private fun updateTotalTime(newTime: Int) {
        supportActionBar?.subtitle = "New Brew - ${newTime}s"
    }

    private fun saveBrew() {
        bloomTime =
            if (binding.aeropressBrewBloomTimeLayout.isEditTextEmpty()) 0 else binding.aeropressBrewBloomTimeLayout.editText?.text.toString().toInt()
        steepTime = binding.aeropressBrewSteepTimeLayout.editText?.text.toString().toInt()
        coffeeWeight = binding.aeropressBrewCoffeeMassLayout.editText?.text.toString().toFloat()
        waterWeight = binding.aeropressBrewWaterMassLayout.editText?.text.toString().toFloat()

        val brew = Aeropress(null, selectedBean, bIsInverted, bloomTime, coffeeWeight, waterWeight, steepTime)

        val aModel = ViewModelProvider(this)[AeropressViewModel::class.java]
        aModel.addNewBrew(brew)
    }

    private fun initBeansList() {
        beanVm = ViewModelProvider(this).get(BeanViewModel::class.java)
        beanVm.allBeans.observe(this, Observer {
            val adapter = ArrayAdapter(this, R.layout.bean_dropdown_item, it)
            (binding.aeropressChooseBeanLayout.editText as? AutoCompleteTextView)?.setAdapter(adapter)
            (binding.aeropressChooseBeanLayout.editText as? AutoCompleteTextView)?.setOnItemClickListener { _, _, position, _ ->
                // TODO dangerous
                selectedBean = adapter.getItem(position)!!
            }
        })
    }

    private fun validateRequiredInputs(): Boolean {
        return (!binding.aeropressChooseBeanLayout.isEditTextEmpty()) &&
                (!binding.aeropressBrewCoffeeMassLayout.isEditTextEmpty()) &&
                !binding.aeropressBrewWaterMassLayout.isEditTextEmpty() &&
                !binding.aeropressBrewSteepTimeLayout.isEditTextEmpty()
    }

    private fun TextInputLayout.isEditTextEmpty(): Boolean =
        this.editText?.text?.toString() == ""
}